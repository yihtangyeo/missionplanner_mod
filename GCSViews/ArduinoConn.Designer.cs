﻿namespace MissionPlanner.GCSViews
{
    partial class ArduinoConn
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ArduinoConn));
            this.BUT_ConnectArduino = new MissionPlanner.Controls.MyButton();
            this.BUT_disconnectArduino = new MissionPlanner.Controls.MyButton();
            this.CMB_boardtype = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // BUT_ConnectArduino
            // 
            resources.ApplyResources(this.BUT_ConnectArduino, "BUT_ConnectArduino");
            this.BUT_ConnectArduino.Name = "BUT_ConnectArduino";
            this.BUT_ConnectArduino.UseVisualStyleBackColor = true;
            this.BUT_ConnectArduino.Click += new System.EventHandler(this.BUT_RebootAPM_Click);
            // 
            // BUT_disconnectArduino
            // 
            resources.ApplyResources(this.BUT_disconnectArduino, "BUT_disconnectArduino");
            this.BUT_disconnectArduino.Name = "BUT_disconnectArduino";
            this.BUT_disconnectArduino.UseVisualStyleBackColor = true;
            this.BUT_disconnectArduino.Click += new System.EventHandler(this.BUT_disconnect_Click);
            // 
            // CMB_boardtype
            // 
            this.CMB_boardtype.FormattingEnabled = true;
            this.CMB_boardtype.Items.AddRange(new object[] {
            resources.GetString("CMB_boardtype.Items"),
            resources.GetString("CMB_boardtype.Items1"),
            resources.GetString("CMB_boardtype.Items2"),
            resources.GetString("CMB_boardtype.Items3")});
            resources.ApplyResources(this.CMB_boardtype, "CMB_boardtype");
            this.CMB_boardtype.Name = "CMB_boardtype";
            // 
            // ArduinoConn
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.CMB_boardtype);
            this.Controls.Add(this.BUT_disconnectArduino);
            this.Controls.Add(this.BUT_ConnectArduino);
            this.Name = "ArduinoConn";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Terminal_FormClosing);
            this.Load += new System.EventHandler(this.Terminal_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.MyButton BUT_ConnectArduino;
        private Controls.MyButton BUT_disconnectArduino;
        private System.Windows.Forms.ComboBox CMB_boardtype;
    }
}
