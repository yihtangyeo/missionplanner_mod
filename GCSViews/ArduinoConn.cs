﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using log4net;
using MissionPlanner.Comms;
using MissionPlanner.Controls;
using MissionPlanner.Log;
using MissionPlanner.Utilities;
using SerialPort = MissionPlanner.Comms.SerialPort;

namespace MissionPlanner.GCSViews
{
    public partial class ArduinoConn : MyUserControl, IActivate, IDeactivate
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        internal static ICommsSerial comPort;
        public static bool threadrun;
        private readonly List<string> cmdHistory = new List<string>();
        private readonly object thisLock = new object();
        private int history;
        private bool inlogview;
        private int inputStartPos;
        DateTime lastsend = DateTime.MinValue;

        public ArduinoConn()
        {
            threadrun = false;

            InitializeComponent();
        }

        public void Activate()
        {
            MainV2.instance.MenuConnect.Visible = false;
        }

        public void Deactivate()
        {
            try
            {
                if (comPort.IsOpen)
                {
                    //comPort.Write("\rexit\rreboot\r");

                    comPort.Close();
                }
            }
            catch
            {
            }

            MainV2.instance.MenuConnect.Visible = true;
        }

        private void comPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (!comPort.IsOpen)
                return;

            // if btr > 0 then this shouldnt happen
            comPort.ReadTimeout = 300;

            try
            {
                lock (thisLock)
                {
                    var buffer = new byte[256];
                    var a = 0;

                    while (comPort.IsOpen && comPort.BytesToRead > 0 && !inlogview)
                    {
                        var indata = (byte) comPort.ReadByte();

                        buffer[a] = indata;

                        if (buffer[a] >= 0x20 && buffer[a] < 0x7f || buffer[a] == '\n' || buffer[a] == 0x1b)
                        {
                            a++;
                        }

                        if (indata == '\n')
                            break;

                        if (a == (buffer.Length - 1))
                            break;
                    }

                    addText(Encoding.ASCII.GetString(buffer, 0, a + 1));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                if (!threadrun) return;
            }
        }

        private void addText(string data)
        {
        }

        private void TXT_terminal_Click(object sender, EventArgs e)
        {
            // auto scroll
            //TXT_terminal.SelectionStart = TXT_terminal.Text.Length;

            //TXT_terminal.ScrollToCaret();

            //TXT_terminal.Refresh();
        }


        private void Terminal_FormClosing(object sender, FormClosingEventArgs e)
        {
            threadrun = false;

            try
            {
                if (comPort != null && comPort.IsOpen)
                {
                    comPort.Close();
                }
            }
            catch
            {
            } // Exception System.IO.IOException: The specified port does not exist.

            //System.Threading.Thread.Sleep(400);
        }


        private void waitandsleep(int time)
        {
            var start = DateTime.Now;

            while ((DateTime.Now - start).TotalMilliseconds < time && !inlogview)
            {
                try
                {
                    if (!comPort.IsOpen || comPort.BytesToRead > 0)
                    {
                        return;
                    }
                }
                catch
                {
                    threadrun = false;
                    return;
                }
            }
        }

        private void readandsleep(int time)
        {
            var start = DateTime.Now;

            while ((DateTime.Now - start).TotalMilliseconds < time && !inlogview)
            {
                try
                {
                    if (!comPort.IsOpen)
                        return;
                    if (comPort.BytesToRead > 0)
                    {
                        comPort_DataReceived(null, null);
                    }
                }
                catch
                {
                    threadrun = false;
                    return;
                }
            }
        }

        private void Terminal_Load(object sender, EventArgs e)
        {
            setcomport();
        }

        private void setcomport()
        {
            if (comPort == null)
            {
                try
                {
                    comPort = new SerialPort();
                    comPort.PortName = MainV2.comPortName;
                    comPort.BaudRate = int.Parse(MainV2._connectionControl.CMB_baudrate.Text);
                    comPort.ReadBufferSize = 1024*1024*4;
                }
                catch
                {
                    CustomMessageBox.Show(Strings.InvalidBaudRate, Strings.ERROR);
                }
            }
        }

        private void start_Terminal(bool px4)
        {
        }

        private void startreadthread()
        {
            Console.WriteLine("Terminal_Load run " + threadrun + " " + comPort.IsOpen);

            BUT_disconnectArduino.Enabled = true;

            var t11 = new Thread(delegate()
            {
                threadrun = true;

                Console.WriteLine("Terminal thread start run run " + threadrun + " " + comPort.IsOpen);

                try
                {
                    comPort.Write("\r");
                }
                catch
                {
                }

                // 10 sec
                waitandsleep(10000);

                Console.WriteLine("Terminal thread 1 run " + threadrun + " " + comPort.IsOpen);

                // 100 ms
                readandsleep(100);

                Console.WriteLine("Terminal thread 2 run " + threadrun + " " + comPort.IsOpen);

                try
                {
                    if (!inlogview && comPort.IsOpen)
                        comPort.Write("\n\n\n");

                    // 1 secs
                    if (!inlogview && comPort.IsOpen)
                        readandsleep(1000);

                    if (!inlogview && comPort.IsOpen)
                        comPort.Write("\r\r\r?\r");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Terminal thread 3 " + ex);
                    ChangeConnectStatus(false);
                    threadrun = false;
                    return;
                }

                Console.WriteLine("Terminal thread 3 run " + threadrun + " " + comPort.IsOpen);

                while (threadrun)
                {
                    try
                    {
                        Thread.Sleep(10);

                        if (!threadrun)
                            break;
                        if (this.Disposing)
                            break;
                        if (inlogview)
                            continue;
                        if (!comPort.IsOpen)
                        {
                            Console.WriteLine("Comport Closed");
                            ChangeConnectStatus(false);
                            break;
                        }
                        if (comPort.BytesToRead > 0)
                        {
                            comPort_DataReceived(null, null);
                        }

                        if (comPort is MAVLinkSerialPort)
                        {
                            if (lastsend.AddMilliseconds(500) > DateTime.Now)
                            {
                                // 20 hz
                                ((MAVLinkSerialPort) comPort).timeout = 50;
                            }
                            else
                            {
                                // 5 hz
                                ((MAVLinkSerialPort) comPort).timeout = 200;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Terminal thread 4 " + ex);
                    }
                }

                threadrun = false;
                try
                {
                    comPort.DtrEnable = false;
                }
                catch
                {
                }
                try
                {
                    Console.WriteLine("term thread close run " + threadrun + " " + comPort.IsOpen);
                    ChangeConnectStatus(false);
                    comPort.Close();
                }
                catch
                {
                }

                Console.WriteLine("Comport thread close run " + threadrun);
            });
            t11.IsBackground = true;
            t11.Name = "Terminal serial thread";
            t11.Start();

            // doesnt seem to work on mac
            //comPort.DataReceived += new SerialDataReceivedEventHandler(comPort_DataReceived);

            if (IsDisposed || Disposing)
                return;

        }

        private void ChangeConnectStatus(bool connected)
        {
            if (IsDisposed || Disposing)
                return;

            Invoke((MethodInvoker) delegate
            {
                if (connected && BUT_disconnectArduino.Enabled == false)
                {
                    BUT_disconnectArduino.Enabled = true;
                }
                else if (!connected && BUT_disconnectArduino.Enabled)
                {
                    BUT_disconnectArduino.Enabled = false;
                }
            });
        }

        private void Logs_Click(object sender, EventArgs e)
        {
            inlogview = true;
            Thread.Sleep(300);
            Form Log = new LogDownload();
            ThemeManager.ApplyThemeTo(Log);
            Log.ShowDialog();
            inlogview = false;
        }


        private void BUT_RebootAPM_Click(object sender, EventArgs e)
        {
            if (comPort.IsOpen)
            {
                BUT_disconnectArduino.Enabled = true;
                return;
            }

            if (MainV2.comPort.BaseStream.IsOpen)
            {
                if (CMB_boardtype.Text.Contains("NSH"))
                {
                    start_NSHTerminal();
                    return;
                }

                MainV2.comPort.BaseStream.Close();
            }

            if (CMB_boardtype.Text.Contains("APM"))
                start_Terminal(false);
            if (CMB_boardtype.Text.Contains("PX4"))
                start_Terminal(true);
            if (CMB_boardtype.Text.Contains("VRX"))
                start_Terminal(true);
        }

        private void start_NSHTerminal()
        {
            try
            {
                if (MainV2.comPort != null && MainV2.comPort.BaseStream != null && MainV2.comPort.BaseStream.IsOpen)
                {
                    comPort = new MAVLinkSerialPort(MainV2.comPort, MAVLink.SERIAL_CONTROL_DEV.SHELL);

                    comPort.BaudRate = 0;

                    // 20 hz
                    ((MAVLinkSerialPort) comPort).timeout = 50;

                    comPort.Open();

                    startreadthread();
                }
            }
            catch
            {
            }
        }

        private void BUT_disconnect_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    comPort.Write("reboot\n");
                }
                catch
                {
                }
                comPort.Close();
                
            }
            catch
            {
            }
        }
    }
}