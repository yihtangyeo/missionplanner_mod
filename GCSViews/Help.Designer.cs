﻿namespace MissionPlanner.GCSViews
{
    partial class Help
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Help));
            this.BUT_setJoystickCalibration = new MissionPlanner.Controls.MyButton();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_Joystick_MinAngle = new System.Windows.Forms.TextBox();
            this.textBox_Joystick_MaxAngle = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_Joystick_MinPWM = new System.Windows.Forms.TextBox();
            this.textBox_Joystick_MaxPWM = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox_Joystick_MinAnalog = new System.Windows.Forms.TextBox();
            this.textBox_Joystick_MaxAnalog = new System.Windows.Forms.TextBox();
            this.textBox_AntTracker_MaxPWM = new System.Windows.Forms.TextBox();
            this.textBox_AntTracker_MinPWM = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox_AntTracker_MaxAngle = new System.Windows.Forms.TextBox();
            this.textBox_AntTracker_MinAngle = new System.Windows.Forms.TextBox();
            this.chkEnableJoystick = new System.Windows.Forms.CheckBox();
            this.chkEnableAntTracker = new System.Windows.Forms.CheckBox();
            this.cmb_ArduinoPorts = new System.Windows.Forms.ComboBox();
            this.cmb_ArduinoBaud = new System.Windows.Forms.ComboBox();
            this.but_ArduinoConnect = new MissionPlanner.Controls.MyButton();
            this.but_ArduinoDisconnect = new MissionPlanner.Controls.MyButton();
            this.BUT_refreshArduinoPorts = new MissionPlanner.Controls.MyButton();
            this.label8 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox_AntTracker_MaxPWM2 = new System.Windows.Forms.TextBox();
            this.textBox_AntTracker_MinPWM2 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox_AntTracker_MaxAngle2 = new System.Windows.Forms.TextBox();
            this.textBox_AntTracker_MinAngle2 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox_Joystick_MaxAnalog2 = new System.Windows.Forms.TextBox();
            this.textBox_Joystick_MinAnalog2 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox_Joystick_MaxPWM2 = new System.Windows.Forms.TextBox();
            this.textBox_Joystick_MinPWM2 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.textBox_Joystick_MaxAngle2 = new System.Windows.Forms.TextBox();
            this.textBox_Joystick_MinAngle2 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtboxCamPosX = new System.Windows.Forms.TextBox();
            this.txtboxCamPosY = new System.Windows.Forms.TextBox();
            this.txtboxCamPosZ = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.txtboxCamLensPosZ = new System.Windows.Forms.TextBox();
            this.txtboxCamLensPosY = new System.Windows.Forms.TextBox();
            this.txtboxCamLensPosX = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.BUT_setAntTrackerCalibration = new MissionPlanner.Controls.MyButton();
            this.SuspendLayout();
            // 
            // BUT_setJoystickCalibration
            // 
            resources.ApplyResources(this.BUT_setJoystickCalibration, "BUT_setJoystickCalibration");
            this.BUT_setJoystickCalibration.Name = "BUT_setJoystickCalibration";
            this.BUT_setJoystickCalibration.UseVisualStyleBackColor = true;
            this.BUT_setJoystickCalibration.Click += new System.EventHandler(this.BUT_setJoystickCalibration_Click);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // textBox_Joystick_MinAngle
            // 
            resources.ApplyResources(this.textBox_Joystick_MinAngle, "textBox_Joystick_MinAngle");
            this.textBox_Joystick_MinAngle.Name = "textBox_Joystick_MinAngle";
            this.textBox_Joystick_MinAngle.TextChanged += new System.EventHandler(this.textBox_Joystick_MinAngle_TextChanged);
            // 
            // textBox_Joystick_MaxAngle
            // 
            resources.ApplyResources(this.textBox_Joystick_MaxAngle, "textBox_Joystick_MaxAngle");
            this.textBox_Joystick_MaxAngle.Name = "textBox_Joystick_MaxAngle";
            this.textBox_Joystick_MaxAngle.TextChanged += new System.EventHandler(this.textBox_Joystick_MaxAngle_TextChanged);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // textBox_Joystick_MinPWM
            // 
            resources.ApplyResources(this.textBox_Joystick_MinPWM, "textBox_Joystick_MinPWM");
            this.textBox_Joystick_MinPWM.Name = "textBox_Joystick_MinPWM";
            this.textBox_Joystick_MinPWM.TextChanged += new System.EventHandler(this.textBox_Joystick_MinPWM_TextChanged);
            // 
            // textBox_Joystick_MaxPWM
            // 
            resources.ApplyResources(this.textBox_Joystick_MaxPWM, "textBox_Joystick_MaxPWM");
            this.textBox_Joystick_MaxPWM.Name = "textBox_Joystick_MaxPWM";
            this.textBox_Joystick_MaxPWM.TextChanged += new System.EventHandler(this.textBox_Joystick_MaxPWM_TextChanged);
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // textBox_Joystick_MinAnalog
            // 
            resources.ApplyResources(this.textBox_Joystick_MinAnalog, "textBox_Joystick_MinAnalog");
            this.textBox_Joystick_MinAnalog.Name = "textBox_Joystick_MinAnalog";
            this.textBox_Joystick_MinAnalog.TextChanged += new System.EventHandler(this.textBox_Joystick_MinAnalog_TextChanged);
            // 
            // textBox_Joystick_MaxAnalog
            // 
            resources.ApplyResources(this.textBox_Joystick_MaxAnalog, "textBox_Joystick_MaxAnalog");
            this.textBox_Joystick_MaxAnalog.Name = "textBox_Joystick_MaxAnalog";
            this.textBox_Joystick_MaxAnalog.TextChanged += new System.EventHandler(this.textBox_Joystick_MaxAnalog_TextChanged);
            // 
            // textBox_AntTracker_MaxPWM
            // 
            resources.ApplyResources(this.textBox_AntTracker_MaxPWM, "textBox_AntTracker_MaxPWM");
            this.textBox_AntTracker_MaxPWM.Name = "textBox_AntTracker_MaxPWM";
            this.textBox_AntTracker_MaxPWM.TextChanged += new System.EventHandler(this.textBox_AntTracker_MaxPWM_TextChanged);
            // 
            // textBox_AntTracker_MinPWM
            // 
            resources.ApplyResources(this.textBox_AntTracker_MinPWM, "textBox_AntTracker_MinPWM");
            this.textBox_AntTracker_MinPWM.Name = "textBox_AntTracker_MinPWM";
            this.textBox_AntTracker_MinPWM.TextChanged += new System.EventHandler(this.textBox_AntTracker_MinPWM_TextChanged);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            // 
            // textBox_AntTracker_MaxAngle
            // 
            resources.ApplyResources(this.textBox_AntTracker_MaxAngle, "textBox_AntTracker_MaxAngle");
            this.textBox_AntTracker_MaxAngle.Name = "textBox_AntTracker_MaxAngle";
            this.textBox_AntTracker_MaxAngle.TextChanged += new System.EventHandler(this.textBox_AntTracker_MaxAngle_TextChanged);
            // 
            // textBox_AntTracker_MinAngle
            // 
            resources.ApplyResources(this.textBox_AntTracker_MinAngle, "textBox_AntTracker_MinAngle");
            this.textBox_AntTracker_MinAngle.Name = "textBox_AntTracker_MinAngle";
            this.textBox_AntTracker_MinAngle.TextChanged += new System.EventHandler(this.textBox_AntTracker_MinAngle_TextChanged);
            // 
            // chkEnableJoystick
            // 
            resources.ApplyResources(this.chkEnableJoystick, "chkEnableJoystick");
            this.chkEnableJoystick.Name = "chkEnableJoystick";
            this.chkEnableJoystick.UseVisualStyleBackColor = true;
            this.chkEnableJoystick.CheckedChanged += new System.EventHandler(this.chkEnableJoystick_CheckedChanged);
            // 
            // chkEnableAntTracker
            // 
            resources.ApplyResources(this.chkEnableAntTracker, "chkEnableAntTracker");
            this.chkEnableAntTracker.Name = "chkEnableAntTracker";
            this.chkEnableAntTracker.UseVisualStyleBackColor = true;
            this.chkEnableAntTracker.CheckedChanged += new System.EventHandler(this.chkEnableAntTracker_CheckedChanged);
            // 
            // cmb_ArduinoPorts
            // 
            this.cmb_ArduinoPorts.FormattingEnabled = true;
            resources.ApplyResources(this.cmb_ArduinoPorts, "cmb_ArduinoPorts");
            this.cmb_ArduinoPorts.Name = "cmb_ArduinoPorts";
            // 
            // cmb_ArduinoBaud
            // 
            this.cmb_ArduinoBaud.FormattingEnabled = true;
            resources.ApplyResources(this.cmb_ArduinoBaud, "cmb_ArduinoBaud");
            this.cmb_ArduinoBaud.Name = "cmb_ArduinoBaud";
            // 
            // but_ArduinoConnect
            // 
            resources.ApplyResources(this.but_ArduinoConnect, "but_ArduinoConnect");
            this.but_ArduinoConnect.Name = "but_ArduinoConnect";
            this.but_ArduinoConnect.UseVisualStyleBackColor = true;
            this.but_ArduinoConnect.Click += new System.EventHandler(this.but_ArduinoConnect_Click);
            // 
            // but_ArduinoDisconnect
            // 
            resources.ApplyResources(this.but_ArduinoDisconnect, "but_ArduinoDisconnect");
            this.but_ArduinoDisconnect.Name = "but_ArduinoDisconnect";
            this.but_ArduinoDisconnect.UseVisualStyleBackColor = true;
            this.but_ArduinoDisconnect.Click += new System.EventHandler(this.but_ArduinoDisconnect_Click);
            // 
            // BUT_refreshArduinoPorts
            // 
            resources.ApplyResources(this.BUT_refreshArduinoPorts, "BUT_refreshArduinoPorts");
            this.BUT_refreshArduinoPorts.Name = "BUT_refreshArduinoPorts";
            this.BUT_refreshArduinoPorts.UseVisualStyleBackColor = true;
            this.BUT_refreshArduinoPorts.Click += new System.EventHandler(this.BUT_refreshArduinoPorts_Click);
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.Name = "label13";
            // 
            // textBox_AntTracker_MaxPWM2
            // 
            resources.ApplyResources(this.textBox_AntTracker_MaxPWM2, "textBox_AntTracker_MaxPWM2");
            this.textBox_AntTracker_MaxPWM2.Name = "textBox_AntTracker_MaxPWM2";
            this.textBox_AntTracker_MaxPWM2.TextChanged += new System.EventHandler(this.textBox_AntTracker_MaxPWM2_TextChanged);
            // 
            // textBox_AntTracker_MinPWM2
            // 
            resources.ApplyResources(this.textBox_AntTracker_MinPWM2, "textBox_AntTracker_MinPWM2");
            this.textBox_AntTracker_MinPWM2.Name = "textBox_AntTracker_MinPWM2";
            this.textBox_AntTracker_MinPWM2.TextChanged += new System.EventHandler(this.textBox_AntTracker_MinPWM2_TextChanged);
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.Name = "label14";
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.Name = "label15";
            // 
            // label16
            // 
            resources.ApplyResources(this.label16, "label16");
            this.label16.Name = "label16";
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.Name = "label17";
            // 
            // textBox_AntTracker_MaxAngle2
            // 
            resources.ApplyResources(this.textBox_AntTracker_MaxAngle2, "textBox_AntTracker_MaxAngle2");
            this.textBox_AntTracker_MaxAngle2.Name = "textBox_AntTracker_MaxAngle2";
            this.textBox_AntTracker_MaxAngle2.TextChanged += new System.EventHandler(this.textBox_AntTracker_MaxAngle2_TextChanged);
            // 
            // textBox_AntTracker_MinAngle2
            // 
            resources.ApplyResources(this.textBox_AntTracker_MinAngle2, "textBox_AntTracker_MinAngle2");
            this.textBox_AntTracker_MinAngle2.Name = "textBox_AntTracker_MinAngle2";
            this.textBox_AntTracker_MinAngle2.TextChanged += new System.EventHandler(this.textBox_AntTracker_MinAngle2_TextChanged);
            // 
            // label18
            // 
            resources.ApplyResources(this.label18, "label18");
            this.label18.Name = "label18";
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.Name = "label19";
            // 
            // textBox_Joystick_MaxAnalog2
            // 
            resources.ApplyResources(this.textBox_Joystick_MaxAnalog2, "textBox_Joystick_MaxAnalog2");
            this.textBox_Joystick_MaxAnalog2.Name = "textBox_Joystick_MaxAnalog2";
            this.textBox_Joystick_MaxAnalog2.TextChanged += new System.EventHandler(this.textBox_Joystick_MaxAnalog2_TextChanged);
            // 
            // textBox_Joystick_MinAnalog2
            // 
            resources.ApplyResources(this.textBox_Joystick_MinAnalog2, "textBox_Joystick_MinAnalog2");
            this.textBox_Joystick_MinAnalog2.Name = "textBox_Joystick_MinAnalog2";
            this.textBox_Joystick_MinAnalog2.TextChanged += new System.EventHandler(this.textBox_Joystick_MinAnalog2_TextChanged);
            // 
            // label20
            // 
            resources.ApplyResources(this.label20, "label20");
            this.label20.Name = "label20";
            // 
            // textBox_Joystick_MaxPWM2
            // 
            resources.ApplyResources(this.textBox_Joystick_MaxPWM2, "textBox_Joystick_MaxPWM2");
            this.textBox_Joystick_MaxPWM2.Name = "textBox_Joystick_MaxPWM2";
            this.textBox_Joystick_MaxPWM2.TextChanged += new System.EventHandler(this.textBox_Joystick_MaxPWM2_TextChanged);
            // 
            // textBox_Joystick_MinPWM2
            // 
            resources.ApplyResources(this.textBox_Joystick_MinPWM2, "textBox_Joystick_MinPWM2");
            this.textBox_Joystick_MinPWM2.Name = "textBox_Joystick_MinPWM2";
            this.textBox_Joystick_MinPWM2.TextChanged += new System.EventHandler(this.textBox_Joystick_MinPWM2_TextChanged);
            // 
            // label21
            // 
            resources.ApplyResources(this.label21, "label21");
            this.label21.Name = "label21";
            // 
            // label22
            // 
            resources.ApplyResources(this.label22, "label22");
            this.label22.Name = "label22";
            // 
            // label23
            // 
            resources.ApplyResources(this.label23, "label23");
            this.label23.Name = "label23";
            // 
            // label24
            // 
            resources.ApplyResources(this.label24, "label24");
            this.label24.Name = "label24";
            // 
            // textBox_Joystick_MaxAngle2
            // 
            resources.ApplyResources(this.textBox_Joystick_MaxAngle2, "textBox_Joystick_MaxAngle2");
            this.textBox_Joystick_MaxAngle2.Name = "textBox_Joystick_MaxAngle2";
            this.textBox_Joystick_MaxAngle2.TextChanged += new System.EventHandler(this.textBox_Joystick_MaxAngle2_TextChanged);
            // 
            // textBox_Joystick_MinAngle2
            // 
            resources.ApplyResources(this.textBox_Joystick_MinAngle2, "textBox_Joystick_MinAngle2");
            this.textBox_Joystick_MinAngle2.Name = "textBox_Joystick_MinAngle2";
            this.textBox_Joystick_MinAngle2.TextChanged += new System.EventHandler(this.textBox_Joystick_MinAngle2_TextChanged);
            // 
            // label25
            // 
            resources.ApplyResources(this.label25, "label25");
            this.label25.Name = "label25";
            // 
            // label26
            // 
            resources.ApplyResources(this.label26, "label26");
            this.label26.Name = "label26";
            // 
            // label27
            // 
            resources.ApplyResources(this.label27, "label27");
            this.label27.Name = "label27";
            // 
            // txtboxCamPosX
            // 
            resources.ApplyResources(this.txtboxCamPosX, "txtboxCamPosX");
            this.txtboxCamPosX.Name = "txtboxCamPosX";
            this.txtboxCamPosX.TextChanged += new System.EventHandler(this.txtboxCamPosX_TextChanged);
            // 
            // txtboxCamPosY
            // 
            resources.ApplyResources(this.txtboxCamPosY, "txtboxCamPosY");
            this.txtboxCamPosY.Name = "txtboxCamPosY";
            this.txtboxCamPosY.TextChanged += new System.EventHandler(this.txtboxCamPosY_TextChanged);
            // 
            // txtboxCamPosZ
            // 
            resources.ApplyResources(this.txtboxCamPosZ, "txtboxCamPosZ");
            this.txtboxCamPosZ.Name = "txtboxCamPosZ";
            this.txtboxCamPosZ.TextChanged += new System.EventHandler(this.txtboxCamPosZ_TextChanged);
            // 
            // label28
            // 
            resources.ApplyResources(this.label28, "label28");
            this.label28.Name = "label28";
            // 
            // label29
            // 
            resources.ApplyResources(this.label29, "label29");
            this.label29.Name = "label29";
            // 
            // label30
            // 
            resources.ApplyResources(this.label30, "label30");
            this.label30.Name = "label30";
            // 
            // label31
            // 
            resources.ApplyResources(this.label31, "label31");
            this.label31.Name = "label31";
            // 
            // label32
            // 
            resources.ApplyResources(this.label32, "label32");
            this.label32.Name = "label32";
            // 
            // label33
            // 
            resources.ApplyResources(this.label33, "label33");
            this.label33.Name = "label33";
            // 
            // label34
            // 
            resources.ApplyResources(this.label34, "label34");
            this.label34.Name = "label34";
            // 
            // label35
            // 
            resources.ApplyResources(this.label35, "label35");
            this.label35.Name = "label35";
            // 
            // label36
            // 
            resources.ApplyResources(this.label36, "label36");
            this.label36.Name = "label36";
            // 
            // label37
            // 
            resources.ApplyResources(this.label37, "label37");
            this.label37.Name = "label37";
            // 
            // label38
            // 
            resources.ApplyResources(this.label38, "label38");
            this.label38.Name = "label38";
            // 
            // label39
            // 
            resources.ApplyResources(this.label39, "label39");
            this.label39.Name = "label39";
            // 
            // label40
            // 
            resources.ApplyResources(this.label40, "label40");
            this.label40.Name = "label40";
            // 
            // label41
            // 
            resources.ApplyResources(this.label41, "label41");
            this.label41.Name = "label41";
            // 
            // label42
            // 
            resources.ApplyResources(this.label42, "label42");
            this.label42.Name = "label42";
            // 
            // txtboxCamLensPosZ
            // 
            resources.ApplyResources(this.txtboxCamLensPosZ, "txtboxCamLensPosZ");
            this.txtboxCamLensPosZ.Name = "txtboxCamLensPosZ";
            this.txtboxCamLensPosZ.TextChanged += new System.EventHandler(this.txtboxCamLensPosZ_TextChanged);
            // 
            // txtboxCamLensPosY
            // 
            resources.ApplyResources(this.txtboxCamLensPosY, "txtboxCamLensPosY");
            this.txtboxCamLensPosY.Name = "txtboxCamLensPosY";
            this.txtboxCamLensPosY.TextChanged += new System.EventHandler(this.txtboxCamLensPosY_TextChanged);
            // 
            // txtboxCamLensPosX
            // 
            resources.ApplyResources(this.txtboxCamLensPosX, "txtboxCamLensPosX");
            this.txtboxCamLensPosX.Name = "txtboxCamLensPosX";
            this.txtboxCamLensPosX.TextChanged += new System.EventHandler(this.txtboxCamLensPosX_TextChanged);
            // 
            // label43
            // 
            resources.ApplyResources(this.label43, "label43");
            this.label43.Name = "label43";
            // 
            // label44
            // 
            resources.ApplyResources(this.label44, "label44");
            this.label44.Name = "label44";
            // 
            // label45
            // 
            resources.ApplyResources(this.label45, "label45");
            this.label45.Name = "label45";
            // 
            // label46
            // 
            resources.ApplyResources(this.label46, "label46");
            this.label46.Name = "label46";
            // 
            // label47
            // 
            resources.ApplyResources(this.label47, "label47");
            this.label47.Name = "label47";
            // 
            // label48
            // 
            resources.ApplyResources(this.label48, "label48");
            this.label48.Name = "label48";
            // 
            // label49
            // 
            resources.ApplyResources(this.label49, "label49");
            this.label49.Name = "label49";
            // 
            // label50
            // 
            resources.ApplyResources(this.label50, "label50");
            this.label50.Name = "label50";
            // 
            // label51
            // 
            resources.ApplyResources(this.label51, "label51");
            this.label51.Name = "label51";
            // 
            // BUT_setAntTrackerCalibration
            // 
            resources.ApplyResources(this.BUT_setAntTrackerCalibration, "BUT_setAntTrackerCalibration");
            this.BUT_setAntTrackerCalibration.Name = "BUT_setAntTrackerCalibration";
            this.BUT_setAntTrackerCalibration.UseVisualStyleBackColor = true;
            this.BUT_setAntTrackerCalibration.Click += new System.EventHandler(this.BUT_setAntTrackerCalibration_Click);
            // 
            // Help
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label51);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.label49);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.txtboxCamLensPosZ);
            this.Controls.Add(this.txtboxCamLensPosY);
            this.Controls.Add(this.txtboxCamLensPosX);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.txtboxCamPosZ);
            this.Controls.Add(this.txtboxCamPosY);
            this.Controls.Add(this.txtboxCamPosX);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.textBox_Joystick_MaxAnalog2);
            this.Controls.Add(this.textBox_Joystick_MinAnalog2);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.textBox_Joystick_MaxPWM2);
            this.Controls.Add(this.textBox_Joystick_MinPWM2);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.textBox_Joystick_MaxAngle2);
            this.Controls.Add(this.textBox_Joystick_MinAngle2);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.textBox_AntTracker_MaxPWM2);
            this.Controls.Add(this.textBox_AntTracker_MinPWM2);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.textBox_AntTracker_MaxAngle2);
            this.Controls.Add(this.textBox_AntTracker_MinAngle2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.BUT_refreshArduinoPorts);
            this.Controls.Add(this.but_ArduinoDisconnect);
            this.Controls.Add(this.but_ArduinoConnect);
            this.Controls.Add(this.cmb_ArduinoBaud);
            this.Controls.Add(this.cmb_ArduinoPorts);
            this.Controls.Add(this.chkEnableAntTracker);
            this.Controls.Add(this.chkEnableJoystick);
            this.Controls.Add(this.textBox_AntTracker_MaxPWM);
            this.Controls.Add(this.textBox_AntTracker_MinPWM);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.textBox_AntTracker_MaxAngle);
            this.Controls.Add(this.textBox_AntTracker_MinAngle);
            this.Controls.Add(this.textBox_Joystick_MaxAnalog);
            this.Controls.Add(this.textBox_Joystick_MinAnalog);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBox_Joystick_MaxPWM);
            this.Controls.Add(this.textBox_Joystick_MinPWM);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox_Joystick_MaxAngle);
            this.Controls.Add(this.textBox_Joystick_MinAngle);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BUT_setAntTrackerCalibration);
            this.Controls.Add(this.BUT_setJoystickCalibration);
            this.Name = "Help";
            this.Load += new System.EventHandler(this.Help_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.MyButton BUT_setJoystickCalibration;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_Joystick_MinAngle;
        private System.Windows.Forms.TextBox textBox_Joystick_MaxAngle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_Joystick_MinPWM;
        private System.Windows.Forms.TextBox textBox_Joystick_MaxPWM;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox_Joystick_MinAnalog;
        private System.Windows.Forms.TextBox textBox_Joystick_MaxAnalog;
        private System.Windows.Forms.TextBox textBox_AntTracker_MaxPWM;
        private System.Windows.Forms.TextBox textBox_AntTracker_MinPWM;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox_AntTracker_MaxAngle;
        private System.Windows.Forms.TextBox textBox_AntTracker_MinAngle;
        private System.Windows.Forms.CheckBox chkEnableJoystick;
        private System.Windows.Forms.CheckBox chkEnableAntTracker;
        private System.Windows.Forms.ComboBox cmb_ArduinoPorts;
        private System.Windows.Forms.ComboBox cmb_ArduinoBaud;
        private Controls.MyButton but_ArduinoConnect;
        private Controls.MyButton but_ArduinoDisconnect;
        private Controls.MyButton BUT_refreshArduinoPorts;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBox_AntTracker_MaxPWM2;
        private System.Windows.Forms.TextBox textBox_AntTracker_MinPWM2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox_AntTracker_MaxAngle2;
        private System.Windows.Forms.TextBox textBox_AntTracker_MinAngle2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox_Joystick_MaxAnalog2;
        private System.Windows.Forms.TextBox textBox_Joystick_MinAnalog2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox_Joystick_MaxPWM2;
        private System.Windows.Forms.TextBox textBox_Joystick_MinPWM2;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox textBox_Joystick_MaxAngle2;
        private System.Windows.Forms.TextBox textBox_Joystick_MinAngle2;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtboxCamPosX;
        private System.Windows.Forms.TextBox txtboxCamPosY;
        private System.Windows.Forms.TextBox txtboxCamPosZ;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtboxCamLensPosZ;
        private System.Windows.Forms.TextBox txtboxCamLensPosY;
        private System.Windows.Forms.TextBox txtboxCamLensPosX;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private Controls.MyButton BUT_setAntTrackerCalibration;



    }
}
