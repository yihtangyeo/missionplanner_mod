﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using MissionPlanner.Controls;
using MissionPlanner.Properties;
using MissionPlanner.Comms;
using MissionPlanner.Utilities;

namespace MissionPlanner.GCSViews
{

    // modified by YihTang Yeo 
    // email: yihtang.yeo@gmail.com

    // sorry that I have to delete the help page and modify it instead of creating a new page
    // given my limited knowledge on using the UI in C#. This will be modified in the future if necessary.

    public partial class Help : MyUserControl, IActivate
    {

        public float homeLat;
        public float homeLon;
        public float homeBearing;

        public Help()
        {
            InitializeComponent();
            GetSerialPorts();
            cmb_ArduinoBaud.SelectedItem = "9600";

            if (MainV2.arduinoSerial.IsOpen)
            {
                but_ArduinoConnect.Enabled = false;
                but_ArduinoDisconnect.Enabled = true;
            }
            else
            {
                but_ArduinoConnect.Enabled = true;
                but_ArduinoDisconnect.Enabled = false;
            }

            if (MainV2.enableJoystickCamera)
                chkEnableJoystick.Checked = true;
            else
                chkEnableJoystick.Checked = false;

            if (MainV2.enableAntennaTracker)
                chkEnableAntTracker.Checked = true;
            else
                chkEnableAntTracker.Checked = false;


            string settingJoystick1minServo     = MainV2.ReadSetting("Joystick_1_minServo");
            string settingJoystick1maxServo     = MainV2.ReadSetting("Joystick_1_maxServo");
            string settingJoystick1minPWM       = MainV2.ReadSetting("Joystick_1_minPWM");
            string settingJoystick1maxPWM       = MainV2.ReadSetting("Joystick_1_maxPWM");
            string settingJoystick1minAnalog    = MainV2.ReadSetting("Joystick_1_minAnalog");
            string settingJoystick1maxAnalog    = MainV2.ReadSetting("Joystick_1_maxAnalog");
            string settingJoystick2minServo     = MainV2.ReadSetting("Joystick_2_minServo");
            string settingJoystick2maxServo     = MainV2.ReadSetting("Joystick_2_maxServo");
            string settingJoystick2minPWM       = MainV2.ReadSetting("Joystick_2_minPWM");
            string settingJoystick2maxPWM       = MainV2.ReadSetting("Joystick_2_maxPWM");
            string settingJoystick2minAnalog    = MainV2.ReadSetting("Joystick_2_minAnalog");
            string settingJoystick2maxAnalog    = MainV2.ReadSetting("Joystick_2_maxAnalog");
            string settingJoystickCamPosX       = MainV2.ReadSetting("Joystick_Cam_PosX");
            string settingJoystickCamPosY       = MainV2.ReadSetting("Joystick_Cam_PosY");
            string settingJoystickCamPosZ       = MainV2.ReadSetting("Joystick_Cam_PosZ");
            string settingJoystickCamLensPosX   = MainV2.ReadSetting("Joystick_CamLens_PosX");
            string settingJoystickCamLensPosY   = MainV2.ReadSetting("Joystick_CamLens_PosY");
            string settingJoystickCamLensPosZ   = MainV2.ReadSetting("Joystick_CamLens_PosZ");

            if (settingJoystickCamLensPosX.Equals("Not Found"))
            {
                txtboxCamLensPosX.Text = "0.0";
            }
            else
            {
                txtboxCamLensPosX.Text = settingJoystickCamLensPosX;
            }

            if (settingJoystickCamLensPosY.Equals("Not Found"))
            {
                txtboxCamLensPosY.Text = "0.0";
            }
            else
            {
                txtboxCamLensPosY.Text = settingJoystickCamLensPosY;
            }

            if (settingJoystickCamLensPosZ.Equals("Not Found"))
            {
                txtboxCamLensPosZ.Text = "0.0";
            }
            else
            {
                txtboxCamLensPosZ.Text = settingJoystickCamLensPosZ;
            }

            if (settingJoystickCamPosX.Equals("Not Found"))
            {
                txtboxCamPosX.Text = "0.0";
            }
            else
            {
                txtboxCamPosX.Text = settingJoystickCamPosX;
            }

            if (settingJoystickCamPosY.Equals("Not Found"))
            {
                txtboxCamPosY.Text = "0.0";
            }
            else
            {
                txtboxCamPosY.Text = settingJoystickCamPosY;
            }

            if (settingJoystickCamPosZ.Equals("Not Found"))
            {
                txtboxCamPosZ.Text = "0.0";
            }
            else
            {
                txtboxCamPosZ.Text = settingJoystickCamPosZ;
            }

            if (settingJoystick1minServo.Equals("Not Found"))
            {
                textBox_Joystick_MinAngle.Text = "-90";
            }
            else
            {
                textBox_Joystick_MinAngle.Text = settingJoystick1minServo;
            }

            if (settingJoystick1maxServo.Equals("Not Found"))
            {
                textBox_Joystick_MaxAngle.Text = "90";
            }
            else
            {
                textBox_Joystick_MaxAngle.Text = settingJoystick1maxServo;
            }

            if (settingJoystick1minPWM.Equals("Not Found"))
            {
                textBox_Joystick_MinPWM.Text = "1100";
            }
            else
            {
                textBox_Joystick_MinPWM.Text = settingJoystick1minPWM;
            }

            if (settingJoystick1maxPWM.Equals("Not Found"))
            {
                textBox_Joystick_MaxPWM.Text = "1900";
            }
            else
            {
                textBox_Joystick_MaxPWM.Text = settingJoystick1maxPWM;
            }

            if (settingJoystick1minAnalog.Equals("Not Found"))
            {
                textBox_Joystick_MinAnalog.Text = "0";
            }
            else
            {
                textBox_Joystick_MinAnalog.Text = settingJoystick1minAnalog;
            }

            if (settingJoystick1maxAnalog.Equals("Not Found"))
            {
                textBox_Joystick_MaxAnalog.Text = "99";
            }
            else
            {
                textBox_Joystick_MaxAnalog.Text = settingJoystick1maxAnalog;
            }

            if (settingJoystick2minServo.Equals("Not Found"))
            {
                textBox_Joystick_MinAngle2.Text = "0";
            }
            else
            {
                textBox_Joystick_MinAngle2.Text = settingJoystick2minServo;
            }

            if (settingJoystick2maxServo.Equals("Not Found"))
            {
                textBox_Joystick_MaxAngle2.Text = "90";
            }
            else
            {
                textBox_Joystick_MaxAngle2.Text = settingJoystick2maxServo;
            }

            if (settingJoystick2minPWM.Equals("Not Found"))
            {
                textBox_Joystick_MinPWM2.Text = "1100";
            }
            else
            {
                textBox_Joystick_MinPWM2.Text = settingJoystick2minPWM;
            }

            if (settingJoystick2maxPWM.Equals("Not Found"))
            {
                textBox_Joystick_MaxPWM2.Text = "1900";
            }
            else
            {
                textBox_Joystick_MaxPWM2.Text = settingJoystick2maxPWM;
            }

            if (settingJoystick2minAnalog.Equals("Not Found"))
            {
                textBox_Joystick_MinAnalog2.Text = "0";
            }
            else
            {
                textBox_Joystick_MinAnalog2.Text = settingJoystick2minAnalog;
            }

            if (settingJoystick2maxAnalog.Equals("Not Found"))
            {
                textBox_Joystick_MaxAnalog2.Text = "99";
            }
            else
            {
                textBox_Joystick_MaxAnalog2.Text = settingJoystick2maxAnalog;
            }
            BUT_setJoystickCalibration_Click(null, null);
            
            
            string settingAntTracker1minServo   = MainV2.ReadSetting("AntTracker_1_minServo");
            string settingAntTracker1maxServo   = MainV2.ReadSetting("AntTracker_1_maxServo");
            string settingAntTracker1minPWM     = MainV2.ReadSetting("AntTracker_1_minPWM");
            string settingAntTracker1maxPWM     = MainV2.ReadSetting("AntTracker_1_maxPWM");
            string settingAntTracker2minServo   = MainV2.ReadSetting("AntTracker_2_minServo");
            string settingAntTracker2maxServo   = MainV2.ReadSetting("AntTracker_2_maxServo");
            string settingAntTracker2minPWM     = MainV2.ReadSetting("AntTracker_2_minPWM");
            string settingAntTracker2maxPWM     = MainV2.ReadSetting("AntTracker_2_maxPWM");

            if (settingAntTracker1minServo.Equals("Not Found"))
            {
                textBox_AntTracker_MinAngle.Text = "-90";
            }
            else
            {
                textBox_AntTracker_MinAngle.Text = settingAntTracker1minServo;
            }

            if (settingAntTracker1maxServo.Equals("Not Found"))
            {
                textBox_AntTracker_MaxAngle.Text = "90";
            }
            else
            {
                textBox_AntTracker_MaxAngle.Text = settingAntTracker1maxServo;
            }

            if (settingAntTracker1minPWM.Equals("Not Found"))
            {
                textBox_AntTracker_MinPWM.Text = "1100";
            }
            else
            {
                textBox_AntTracker_MinPWM.Text = settingAntTracker1minPWM;
            }

            if (settingAntTracker1maxPWM.Equals("Not Found"))
            {
                textBox_AntTracker_MaxPWM.Text = "1900";
            }
            else
            {
                textBox_AntTracker_MaxPWM.Text = settingAntTracker1maxPWM;
            }

            if (settingAntTracker2minServo.Equals("Not Found"))
            {
                textBox_AntTracker_MinAngle2.Text = "0";
            }
            else
            {
                textBox_AntTracker_MinAngle2.Text = settingAntTracker2minServo;
            }

            if (settingAntTracker2maxServo.Equals("Not Found"))
            {
                textBox_AntTracker_MaxAngle2.Text = "90";
            }
            else
            {
                textBox_AntTracker_MaxAngle2.Text = settingAntTracker2maxServo;
            }

            if (settingAntTracker2minPWM.Equals("Not Found"))
            {
                textBox_AntTracker_MinPWM2.Text = "1100";
            }
            else
            {
                textBox_AntTracker_MinPWM2.Text = settingAntTracker2minPWM;
            }

            if (settingAntTracker2maxPWM.Equals("Not Found"))
            {
                textBox_AntTracker_MaxPWM2.Text = "1900";
            }
            else
            {
                textBox_AntTracker_MaxPWM2.Text = settingAntTracker2maxPWM;
            }
            BUT_setAntTrackerCalibration_Click(null, null);

        }

        public void Activate()
        {
            try
            {
                
            }
            catch
            {
            }
        }

        private void GetSerialPorts()
        {
            var ports = SerialPort.GetPortNames();
            cmb_ArduinoPorts.DataSource = ports;
            cmb_ArduinoBaud.Items.AddRange(new object[] {"1200",
                        "2400",
                        "4800",
                        "9600",
                        "19200",
                        "38400",
                        "57600",
                        "115200"});
            
        }


        private void Help_Load(object sender, EventArgs e)
        {
            //richTextBox1.Rtf = Resources.help_text;
            //ThemeManager.ApplyThemeTo(richTextBox1);
        }


        private bool IsOKForDecimalTextBox(char theCharacter, TextBox theTextBox, int charIndex)
        {
            // Only allow control characters, digits, plus and minus signs.
            // Only allow ONE plus sign.
            // Only allow ONE minus sign.
            // Only allow the plus or minus sign as the FIRST character.
            // Only allow ONE decimal point.
            // Do NOT allow decimal point or digits BEFORE any plus or minus sign.

            if (
                !char.IsControl(theCharacter)
                && !char.IsDigit(theCharacter)
                && (theCharacter != '.')
                && (theCharacter != '-')
                && (theCharacter != '+')
            )
            {
                // Then it is NOT a character we want allowed in the text box.
                return false;
            }



            // Only allow one decimal point.
            if (theCharacter == '.'
                && theTextBox.Text.IndexOf('.') < charIndex)
            {
                // Then there is already a decimal point in the text box.
                return false;
            }

            // Only allow one minus sign.
            if (theCharacter == '-'
                && theTextBox.Text.IndexOf('-') > 0)
            {
                // Then there is already a minus sign in the text box.
                return false;
            }

            // Only allow one plus sign.
            if (theCharacter == '+'
                && theTextBox.Text.IndexOf('+') > 0)
            {
                // Then there is already a plus sign in the text box.
                return false;
            }


            // Otherwise the character is perfectly fine for a decimal value and the character
            // may indeed be placed at the current insertion position.
            return true;
        }

        private void textBox_Joystick_MinAngle_TextChanged(object sender, EventArgs e)
        {
            if (textBox_Joystick_MinAngle.Text.Length > 0)
            {
                int charIndex = textBox_Joystick_MinAngle.Text.Length - 1;
                char lastChar = textBox_Joystick_MinAngle.Text[charIndex];
                if (IsOKForDecimalTextBox(lastChar, textBox_Joystick_MinAngle, charIndex) == false)
                {
                    MessageBox.Show("Please enter only numbers (negative and decimals are allowed).");
                    textBox_Joystick_MinAngle.Text = textBox_Joystick_MinAngle.Text.Remove(charIndex);
                }
            }
        }

        private void textBox_Joystick_MaxAngle_TextChanged(object sender, EventArgs e)
        {
            if (textBox_Joystick_MaxAngle.Text.Length > 0)
            {
                int charIndex = textBox_Joystick_MaxAngle.Text.Length - 1;
                char lastChar = textBox_Joystick_MaxAngle.Text[charIndex];
                if (IsOKForDecimalTextBox(lastChar, textBox_Joystick_MaxAngle, charIndex) == false)
                {
                    MessageBox.Show("Please enter only numbers (negative and decimals are allowed).");
                    textBox_Joystick_MaxAngle.Text = textBox_Joystick_MaxAngle.Text.Remove(charIndex);
                }
            }
        }

        private void textBox_AntTracker_MinAngle_TextChanged(object sender, EventArgs e)
        {
            if (textBox_AntTracker_MinAngle.Text.Length > 0)
            {
                int charIndex = textBox_AntTracker_MinAngle.Text.Length - 1;
                char lastChar = textBox_AntTracker_MinAngle.Text[charIndex];
                if (IsOKForDecimalTextBox(lastChar, textBox_AntTracker_MinAngle, charIndex) == false)
                {
                    MessageBox.Show("Please enter only numbers (negative and decimals are allowed).");
                    textBox_AntTracker_MinAngle.Text = textBox_AntTracker_MinAngle.Text.Remove(charIndex);
                }
            }
        }

        private void textBox_AntTracker_MaxAngle_TextChanged(object sender, EventArgs e)
        {
            if (textBox_AntTracker_MaxAngle.Text.Length > 0)
            {
                int charIndex = textBox_AntTracker_MaxAngle.Text.Length - 1;
                char lastChar = textBox_AntTracker_MaxAngle.Text[charIndex];
                if (IsOKForDecimalTextBox(lastChar, textBox_AntTracker_MaxAngle, charIndex) == false)
                {
                    MessageBox.Show("Please enter only numbers (negative and decimals are allowed).");
                    textBox_AntTracker_MaxAngle.Text = textBox_AntTracker_MaxAngle.Text.Remove(charIndex);
                }
            }
        }


        private void textBox_Joystick_MinAngle2_TextChanged(object sender, EventArgs e)
        {
            if (textBox_Joystick_MinAngle2.Text.Length > 0)
            {
                int charIndex = textBox_Joystick_MinAngle2.Text.Length - 1;
                char lastChar = textBox_Joystick_MinAngle2.Text[charIndex];
                if (IsOKForDecimalTextBox(lastChar, textBox_Joystick_MinAngle2, charIndex) == false)
                {
                    MessageBox.Show("Please enter only numbers (negative and decimals are allowed).");
                    textBox_Joystick_MinAngle2.Text = textBox_Joystick_MinAngle2.Text.Remove(charIndex);
                }
            }
        }

        private void textBox_Joystick_MaxAngle2_TextChanged(object sender, EventArgs e)
        {
            if (textBox_Joystick_MaxAngle2.Text.Length > 0)
            {
                int charIndex = textBox_Joystick_MaxAngle2.Text.Length - 1;
                char lastChar = textBox_Joystick_MaxAngle2.Text[charIndex];
                if (IsOKForDecimalTextBox(lastChar, textBox_Joystick_MaxAngle2, charIndex) == false)
                {
                    MessageBox.Show("Please enter only numbers (negative and decimals are allowed).");
                    textBox_Joystick_MaxAngle2.Text = textBox_Joystick_MaxAngle2.Text.Remove(charIndex);
                }
            }
        }

        private void textBox_AntTracker_MinAngle2_TextChanged(object sender, EventArgs e)
        {
            if (textBox_AntTracker_MinAngle2.Text.Length > 0)
            {
                int charIndex = textBox_AntTracker_MinAngle2.Text.Length - 1;
                char lastChar = textBox_AntTracker_MinAngle2.Text[charIndex];
                if (IsOKForDecimalTextBox(lastChar, textBox_AntTracker_MinAngle2, charIndex) == false)
                {
                    MessageBox.Show("Please enter only numbers (negative and decimals are allowed).");
                    textBox_AntTracker_MinAngle2.Text = textBox_AntTracker_MinAngle2.Text.Remove(charIndex);
                }
            }
        }

        private void textBox_AntTracker_MaxAngle2_TextChanged(object sender, EventArgs e)
        {
            if (textBox_AntTracker_MaxAngle2.Text.Length > 0)
            {
                int charIndex = textBox_AntTracker_MaxAngle2.Text.Length - 1;
                char lastChar = textBox_AntTracker_MaxAngle2.Text[charIndex];
                if (IsOKForDecimalTextBox(lastChar, textBox_AntTracker_MaxAngle2, charIndex) == false)
                {
                    MessageBox.Show("Please enter only numbers (negative and decimals are allowed).");
                    textBox_AntTracker_MaxAngle2.Text = textBox_AntTracker_MaxAngle2.Text.Remove(charIndex);
                }
            }
        }

        private void textBox_Joystick_MinPWM_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(textBox_Joystick_MinPWM.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers 0-9.");
                textBox_Joystick_MinPWM.Text = textBox_Joystick_MinPWM.Text.Remove(textBox_Joystick_MinPWM.Text.Length - 1);
            }
        }

        private void textBox_Joystick_MaxPWM_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(textBox_Joystick_MaxPWM.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers 0-9.");
                textBox_Joystick_MaxPWM.Text = textBox_Joystick_MaxPWM.Text.Remove(textBox_Joystick_MaxPWM.Text.Length - 1);
            }
        }

        private void textBox_AntTracker_MinPWM_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(textBox_AntTracker_MinPWM.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers 0-9.");
                textBox_AntTracker_MinPWM.Text = textBox_AntTracker_MinPWM.Text.Remove(textBox_AntTracker_MinPWM.Text.Length - 1);
            }
        }

        private void textBox_AntTracker_MaxPWM_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(textBox_AntTracker_MaxPWM.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers 0-9.");
                textBox_AntTracker_MaxPWM.Text = textBox_AntTracker_MaxPWM.Text.Remove(textBox_AntTracker_MaxPWM.Text.Length - 1);
            }
        }

        private void textBox_Joystick_MinAnalog_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(textBox_Joystick_MinAnalog.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers 0-9.");
                textBox_Joystick_MinAnalog.Text = textBox_Joystick_MinAnalog.Text.Remove(textBox_Joystick_MinAnalog.Text.Length - 1);
            }
        }

        private void textBox_Joystick_MaxAnalog_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(textBox_Joystick_MaxAnalog.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers 0-9.");
                textBox_Joystick_MaxAnalog.Text = textBox_Joystick_MaxAnalog.Text.Remove(textBox_Joystick_MaxAnalog.Text.Length - 1);
            }
        }

        private void textBox_Joystick_MinPWM2_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(textBox_Joystick_MinPWM2.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers 0-9.");
                textBox_Joystick_MinPWM2.Text = textBox_Joystick_MinPWM2.Text.Remove(textBox_Joystick_MinPWM2.Text.Length - 1);
            }
        }

        private void textBox_Joystick_MaxPWM2_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(textBox_Joystick_MaxPWM2.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers 0-9.");
                textBox_Joystick_MaxPWM2.Text = textBox_Joystick_MaxPWM2.Text.Remove(textBox_Joystick_MaxPWM2.Text.Length - 1);
            }
        }

        private void textBox_Joystick_MinAnalog2_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(textBox_Joystick_MinAnalog2.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers 0-9.");
                textBox_Joystick_MinAnalog2.Text = textBox_Joystick_MinAnalog2.Text.Remove(textBox_Joystick_MinAnalog2.Text.Length - 1);
            }
        }

        private void textBox_Joystick_MaxAnalog2_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(textBox_Joystick_MaxAnalog2.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers 0-9.");
                textBox_Joystick_MaxAnalog2.Text = textBox_Joystick_MaxAnalog2.Text.Remove(textBox_Joystick_MaxAnalog2.Text.Length - 1);
            }
        }

        private void textBox_AntTracker_MinPWM2_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(textBox_AntTracker_MinPWM2.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers 0-9.");
                textBox_AntTracker_MinPWM2.Text = textBox_AntTracker_MinPWM2.Text.Remove(textBox_AntTracker_MinPWM2.Text.Length - 1);
            }
        }

        private void textBox_AntTracker_MaxPWM2_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(textBox_AntTracker_MaxPWM2.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers 0-9.");
                textBox_AntTracker_MaxPWM2.Text = textBox_AntTracker_MaxPWM2.Text.Remove(textBox_AntTracker_MaxPWM2.Text.Length - 1);
            }
        }


        private void BUT_refreshArduinoPorts_Click(object sender, EventArgs e)
        {
            GetSerialPorts();
        }


        

        private void BUT_setJoystickCalibration_Click(object sender, EventArgs e)
        {
            MainV2.minJoystickServoAngle1 = float.Parse(textBox_Joystick_MinAngle.Text.ToString());
            MainV2.maxJoystickServoAngle1 = float.Parse(textBox_Joystick_MaxAngle.Text.ToString());
            MainV2.minJoystickPWMval1 = Int32.Parse(textBox_Joystick_MinPWM.Text.ToString());
            MainV2.maxJoystickPWMval1 = Int32.Parse(textBox_Joystick_MaxPWM.Text.ToString());
            MainV2.minJoystickAnalogVal1 = Int32.Parse(textBox_Joystick_MinAnalog.Text.ToString());
            MainV2.maxJoystickAnalogVal1 = Int32.Parse(textBox_Joystick_MaxAnalog.Text.ToString());
            MainV2.minJoystickServoAngle2 = float.Parse(textBox_Joystick_MinAngle2.Text.ToString());
            MainV2.maxJoystickServoAngle2 = float.Parse(textBox_Joystick_MaxAngle2.Text.ToString());
            MainV2.minJoystickPWMval2 = Int32.Parse(textBox_Joystick_MinPWM2.Text.ToString());
            MainV2.maxJoystickPWMval2 = Int32.Parse(textBox_Joystick_MaxPWM2.Text.ToString());
            MainV2.minJoystickAnalogVal2 = Int32.Parse(textBox_Joystick_MinAnalog2.Text.ToString());
            MainV2.maxJoystickAnalogVal2 = Int32.Parse(textBox_Joystick_MaxAnalog2.Text.ToString());

            // update settings
            MainV2.AddUpdateAppSettings("Joystick_1_minServo", textBox_Joystick_MinAngle.Text);
            MainV2.AddUpdateAppSettings("Joystick_1_maxServo", textBox_Joystick_MaxAngle.Text);
            MainV2.AddUpdateAppSettings("Joystick_1_minPWM", textBox_Joystick_MinPWM.Text);
            MainV2.AddUpdateAppSettings("Joystick_1_maxPWM", textBox_Joystick_MaxPWM.Text);
            MainV2.AddUpdateAppSettings("Joystick_1_minAnalog", textBox_Joystick_MinAnalog.Text);
            MainV2.AddUpdateAppSettings("Joystick_1_maxAnalog", textBox_Joystick_MaxAnalog.Text);
            MainV2.AddUpdateAppSettings("Joystick_2_minServo", textBox_Joystick_MinAngle2.Text);
            MainV2.AddUpdateAppSettings("Joystick_2_maxServo", textBox_Joystick_MaxAngle2.Text);
            MainV2.AddUpdateAppSettings("Joystick_2_minPWM", textBox_Joystick_MinPWM2.Text);
            MainV2.AddUpdateAppSettings("Joystick_2_maxPWM", textBox_Joystick_MaxPWM2.Text);
            MainV2.AddUpdateAppSettings("Joystick_2_minAnalog", textBox_Joystick_MinAnalog2.Text);
            MainV2.AddUpdateAppSettings("Joystick_2_maxAnalog", textBox_Joystick_MaxAnalog2.Text);
            MainV2.AddUpdateAppSettings("Joystick_Cam_PosX", txtboxCamPosX.Text);
            MainV2.AddUpdateAppSettings("Joystick_Cam_PosY", txtboxCamPosY.Text);
            MainV2.AddUpdateAppSettings("Joystick_Cam_PosZ", txtboxCamPosZ.Text);
            MainV2.AddUpdateAppSettings("Joystick_CamLens_PosX", txtboxCamLensPosX.Text);
            MainV2.AddUpdateAppSettings("Joystick_CamLens_PosY", txtboxCamLensPosY.Text);
            MainV2.AddUpdateAppSettings("Joystick_CamLens_PosZ", txtboxCamLensPosZ.Text);
                        
        }

        

        

        private void BUT_setAntTrackerCalibration_Click(object sender, EventArgs e)
        {
            MainV2.minAntTrackerServoAngle1 = float.Parse(textBox_AntTracker_MinAngle.Text.ToString());
            MainV2.maxAntTrackerServoAngle1 = float.Parse(textBox_AntTracker_MaxAngle.Text.ToString());
            MainV2.minAntTrackerPWMval1 = Int32.Parse(textBox_AntTracker_MinPWM.Text.ToString());
            MainV2.maxAntTrackerPWMval1 = Int32.Parse(textBox_AntTracker_MaxPWM.Text.ToString());
            MainV2.minAntTrackerServoAngle2 = float.Parse(textBox_AntTracker_MinAngle2.Text.ToString());
            MainV2.maxAntTrackerServoAngle2 = float.Parse(textBox_AntTracker_MaxAngle2.Text.ToString());
            MainV2.minAntTrackerPWMval2 = Int32.Parse(textBox_AntTracker_MinPWM2.Text.ToString());
            MainV2.maxAntTrackerPWMval2 = Int32.Parse(textBox_AntTracker_MaxPWM2.Text.ToString());

            // update setting
            MainV2.AddUpdateAppSettings("AntTracker_1_minServo", textBox_AntTracker_MinAngle.Text);
            MainV2.AddUpdateAppSettings("AntTracker_1_maxServo", textBox_AntTracker_MaxAngle.Text);
            MainV2.AddUpdateAppSettings("AntTracker_1_minPWM", textBox_AntTracker_MinPWM.Text);
            MainV2.AddUpdateAppSettings("AntTracker_1_maxPWM", textBox_AntTracker_MaxPWM.Text);
            MainV2.AddUpdateAppSettings("AntTracker_2_minServo", textBox_AntTracker_MinAngle2.Text);
            MainV2.AddUpdateAppSettings("AntTracker_2_maxServo", textBox_AntTracker_MaxAngle2.Text);
            MainV2.AddUpdateAppSettings("AntTracker_2_minPWM", textBox_AntTracker_MinPWM2.Text);
            MainV2.AddUpdateAppSettings("AntTracker_2_maxPWM", textBox_AntTracker_MaxPWM2.Text);

           
        }

         

        private void chkEnableJoystick_CheckedChanged(object sender, EventArgs e)
        {
            if (chkEnableJoystick.Checked)
                MainV2.enableJoystickCamera = true;
            else
                MainV2.enableJoystickCamera = false;
        }

        private void chkEnableAntTracker_CheckedChanged(object sender, EventArgs e)
        {
            MessageBox.Show("Sorry, this feature is not ready yet");
            //if (chkEnableAntTracker.Checked)
            //    MainV2.enableAntennaTracker = true;
            //else
                MainV2.enableAntennaTracker = false;
        }


        private void txtboxCamPosX_TextChanged(object sender, EventArgs e)
        {
            if (txtboxCamPosX.Text.Length > 0)
            {
                int charIndex = txtboxCamPosX.Text.Length - 1;
                char lastChar = txtboxCamPosX.Text[charIndex];
                if (IsOKForDecimalTextBox(lastChar, txtboxCamPosX, charIndex) == false)
                {
                    MessageBox.Show("Please enter only numbers (negative and decimals are allowed).");
                    txtboxCamPosX.Text = txtboxCamPosX.Text.Remove(charIndex);
                }
            }
        }

        private void txtboxCamPosY_TextChanged(object sender, EventArgs e)
        {
            if (txtboxCamPosY.Text.Length > 0)
            {
                int charIndex = txtboxCamPosY.Text.Length - 1;
                char lastChar = txtboxCamPosY.Text[charIndex];
                if (IsOKForDecimalTextBox(lastChar, txtboxCamPosY, charIndex) == false)
                {
                    MessageBox.Show("Please enter only numbers (negative and decimals are allowed).");
                    txtboxCamPosY.Text = txtboxCamPosY.Text.Remove(charIndex);
                }
            }
        }

        private void txtboxCamPosZ_TextChanged(object sender, EventArgs e)
        {
            if (txtboxCamPosZ.Text.Length > 0)
            {
                int charIndex = txtboxCamPosZ.Text.Length - 1;
                char lastChar = txtboxCamPosZ.Text[charIndex];
                if (IsOKForDecimalTextBox(lastChar, txtboxCamPosZ, charIndex) == false)
                {
                    MessageBox.Show("Please enter only numbers (negative and decimals are allowed).");
                    txtboxCamPosZ.Text = txtboxCamPosZ.Text.Remove(charIndex);
                }
            }
        }

        private void txtboxCamLensPosX_TextChanged(object sender, EventArgs e)
        {
            if (txtboxCamLensPosX.Text.Length > 0)
            {
                int charIndex = txtboxCamLensPosX.Text.Length - 1;
                char lastChar = txtboxCamLensPosX.Text[charIndex];
                if (IsOKForDecimalTextBox(lastChar, txtboxCamLensPosX, charIndex) == false)
                {
                    MessageBox.Show("Please enter only numbers (negative and decimals are allowed).");
                    txtboxCamLensPosX.Text = txtboxCamLensPosX.Text.Remove(charIndex);
                }
            }
        }

        private void txtboxCamLensPosY_TextChanged(object sender, EventArgs e)
        {
            if (txtboxCamLensPosY.Text.Length > 0)
            {
                int charIndex = txtboxCamLensPosY.Text.Length - 1;
                char lastChar = txtboxCamLensPosY.Text[charIndex];
                if (IsOKForDecimalTextBox(lastChar, txtboxCamLensPosY, charIndex) == false)
                {
                    MessageBox.Show("Please enter only numbers (negative and decimals are allowed).");
                    txtboxCamLensPosY.Text = txtboxCamLensPosY.Text.Remove(charIndex);
                }
            }
        }

        private void txtboxCamLensPosZ_TextChanged(object sender, EventArgs e)
        {
            if (txtboxCamLensPosZ.Text.Length > 0)
            {
                int charIndex = txtboxCamLensPosZ.Text.Length - 1;
                char lastChar = txtboxCamLensPosZ.Text[charIndex];
                if (IsOKForDecimalTextBox(lastChar, txtboxCamLensPosZ, charIndex) == false)
                {
                    MessageBox.Show("Please enter only numbers (negative and decimals are allowed).");
                    txtboxCamLensPosZ.Text = txtboxCamLensPosZ.Text.Remove(charIndex);
                }
            }
        }

        private void but_ArduinoConnect_Click(object sender, EventArgs e)
        {
            but_ArduinoConnect.Enabled = false;
            but_ArduinoDisconnect.Enabled = true;
            string portName = cmb_ArduinoPorts.Text.ToString();
            int baudRate = int.Parse(cmb_ArduinoBaud.Text.ToString());
            MainV2.ArduinoSerialConnect(portName, baudRate);
        }

        private void but_ArduinoDisconnect_Click(object sender, EventArgs e)
        {
            but_ArduinoConnect.Enabled = true;
            but_ArduinoDisconnect.Enabled = false;
            MainV2.ArduinoSerialDisconnect();
        }

    }

}